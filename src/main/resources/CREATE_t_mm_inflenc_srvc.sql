CREATE TABLE mntnc.t_mm_inflenc_srvc
(
  wo_id character varying(10) NOT NULL, -- 案件ID
  inflenc_service_cd character varying(50) NOT NULL, -- 影響サービスコード
  create_user character varying(32) NOT NULL, -- ▲登録アカウント
  create_date timestamp without time zone NOT NULL, -- ▲登録年月日
  update_user character varying(32) NOT NULL, -- ▲更新アカウント
  update_date timestamp without time zone NOT NULL, -- ▲更新年月日
  del_flg character(1) NOT NULL, -- ▲削除フラグ
  PRIMARY KEY (wo_id , inflenc_service_cd )
) WITHOUT OIDS;