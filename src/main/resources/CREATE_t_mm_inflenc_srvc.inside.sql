COMMENT ON TABLE mntnc.t_mm_inflenc_srvc IS '影響サービス管理';
COMMENT ON COLUMN mntnc.t_mm_inflenc_srvc.wo_id IS '案件ID';
COMMENT ON COLUMN mntnc.t_mm_inflenc_srvc.inflenc_service_cd IS '影響サービスコード';
COMMENT ON COLUMN mntnc.t_mm_inflenc_srvc.create_user IS '▲登録アカウント';
COMMENT ON COLUMN mntnc.t_mm_inflenc_srvc.create_date IS '▲登録年月日';
COMMENT ON COLUMN mntnc.t_mm_inflenc_srvc.update_user IS '▲更新アカウント';
COMMENT ON COLUMN mntnc.t_mm_inflenc_srvc.update_date IS '▲更新年月日';
COMMENT ON COLUMN mntnc.t_mm_inflenc_srvc.del_flg IS '▲削除フラグ';


grant all on mntnc.t_mm_inflenc_srvc to mmuser;