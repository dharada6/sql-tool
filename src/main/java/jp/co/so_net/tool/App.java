package jp.co.so_net.tool;

import jp.co.so_net.tool.exception.RollBackException;
import jp.co.so_net.tool.logic.TransactionLogic;

import org.seasar.framework.container.SingletonS2Container;
import org.seasar.framework.container.factory.SingletonS2ContainerFactory;

/**
 * SQL tool
 * 
 * @author daisuke.harada
 */
public class App {

	public static void main(String[] args) {

		try {

			System.out.println("[START]");

			SingletonS2ContainerFactory.init();
			SingletonS2Container.getComponent(TransactionLogic.class).execute();

			System.out.println("\nOK!");

		} catch (Exception e) {

			if (e instanceof RollBackException) {

				System.out.println("ロールバックしました。");

			} else {

				System.out.println("NG!");
				e.printStackTrace();
			}

		} finally {

			SingletonS2ContainerFactory.destroy();
			System.out.println("[END]");
		}

	}

}
