package jp.co.so_net.tool.exception;

/**
 * 
 */
public class ContinueRuntimeException extends RuntimeException {

	/**
	 * 
	 */
    private static final long serialVersionUID = -7560840992947928445L;

	public ContinueRuntimeException() {
	}

	public ContinueRuntimeException(String message) {
		super(message);
	}

	public ContinueRuntimeException(Throwable cause) {
		super(cause);
	}

	public ContinueRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public ContinueRuntimeException(String message, Throwable cause, boolean enableSuppression,
	        boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
