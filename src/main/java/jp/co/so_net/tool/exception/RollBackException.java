package jp.co.so_net.tool.exception;

/**
 * 
 */
public class RollBackException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5256531076553469599L;

	public RollBackException() {
	}

	public RollBackException(String message) {
		super(message);
	}

	public RollBackException(Throwable cause) {
		super(cause);
	}

	public RollBackException(String message, Throwable cause) {
		super(message, cause);
	}

	public RollBackException(String message, Throwable cause, boolean enableSuppression,
	        boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
