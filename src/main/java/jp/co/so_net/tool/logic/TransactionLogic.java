package jp.co.so_net.tool.logic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import javax.annotation.Resource;

import jp.co.so_net.tool.exception.ContinueRuntimeException;
import jp.co.so_net.tool.exception.RollBackException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.seasar.config.core.container.ConfigContainer;
import org.seasar.extension.jdbc.JdbcManager;
import org.seasar.framework.beans.util.BeanMap;
import org.seasar.util.io.ResourceUtil;

public class TransactionLogic {

	@Resource
	protected JdbcManager jdbcManagerCust;

	@Resource
	protected JdbcManager jdbcManagerMaintenance;

	@Resource
	protected ConfigContainer configContainer;

	/**
	 * SQL実行処理
	 * 
	 * <p>
	 * トランザクション境界となるメソッド
	 * </p>
	 * 
	 * @throws IOException
	 */
	public void execute() throws IOException {

		executeSql("cust_sqlfilelist.txt", jdbcManagerCust);
		executeSql("mntnc_sqlfilelist.txt", jdbcManagerMaintenance);

		// 　標準入力からのcommit or rollbackを受け付ける。
		waitForCommitOrRollback();

	}

	private void executeSql(String sqlfileListTxtFile, JdbcManager jdbcManager) throws IOException {

		for (String sqlFile : FileUtils.readLines(ResourceUtil
		        .getResourceAsFile(sqlfileListTxtFile))) {

			try {

				isComment(trim(sqlFile));

				// リリースSQL本体を実行
				updateSql(jdbcManager, trim(sqlFile));

				// リリース後の確認用SQLを実行
				selectSql(jdbcManager, trim(sqlFile));

			} catch (ContinueRuntimeException e) {

				// not error, just continue
				continue;
			}

		}
	}

	private void waitForCommitOrRollback() throws IOException {

		BufferedReader bufferReader = null;

		try {

			displayPleaseInputMessage();

			bufferReader = new BufferedReader(new InputStreamReader(System.in));

			String line = waitCommitOrRollbackInput(bufferReader);

			if (isEmpty(line)) {
				// Cntl +zを押下された場合、rollbackする。
				throwRollback();

			}

		} finally {
			IOUtils.closeQuietly(bufferReader);
		}
	}

	String waitCommitOrRollbackInput(BufferedReader bufferReader) throws IOException {

		String line = null;

		while ((line = bufferReader.readLine()) != null) {

			if (line.equalsIgnoreCase("yes") || line.equalsIgnoreCase("y")) {

				// commit;
				System.out.println("doing commit!");
				break;

			} else if (line.equalsIgnoreCase("no") || line.equalsIgnoreCase("n")) {

				throwRollback();

			} else {

				displayPleaseInputMessage();
				continue;
			}

		}

		return line;
	}

	void displayPleaseInputMessage() {
		System.out.println("\nCommitしますか？ \"(yes/no)\"");
	}

	private void throwRollback() {

		System.err.println("doing rollback.");
		throw new RollBackException("because of inputted rollback");
	}

	private void updateSql(JdbcManager jdbcManager, String sqlFile) throws IOException {

		if (!sqlFile.contains("_inside.sql") && !sqlFile.startsWith("select")
		        && !sqlFile.startsWith("SELECT")) {

			jdbcManager.updateBySqlFile(trim(sqlFile)).execute();

		} else if (sqlFile.contains("_inside.sql") && !sqlFile.startsWith("select")
		        && !sqlFile.startsWith("SELECT")) {

			for (String sqlStatement : FileUtils.readLines(ResourceUtil.getResourceAsFile(sqlFile))) {

				if (isIgnore(sqlStatement)) {
					continue;
				}

				jdbcManager.updateBySql(trim(sqlStatement)).execute();

			}

		}
	}

	private void selectSql(JdbcManager jdbcManager, String sqlFile) throws IOException {

		if (isSelectSqlNotInside(trim(sqlFile))) {

			systemOutWithSQLFileSelectResult(resultListWithSqlFile(jdbcManager, sqlFile), sqlFile);

		} else if (isSelectSqlInside(trim(sqlFile))) {

			for (String sqlStatement : FileUtils.readLines(ResourceUtil
			        .getResourceAsFile(trim(sqlFile)))) {

				systemOutWithStatement(resultList(jdbcManager, sqlStatement), trim(sqlStatement));

			}

		}
	}

	private List<BeanMap> resultListWithSqlFile(JdbcManager jdbcManager, String sqlFile) {
		return jdbcManager.selectBySqlFile(BeanMap.class, sqlFile).getResultList();
	}

	private List<BeanMap> resultList(JdbcManager jdbcManager, String sqlStatement) {

		if (isIgnore(sqlStatement)) {
			throw new ContinueRuntimeException("because of Empty or comment");
		}

		return jdbcManager.selectBySql(BeanMap.class, trim(sqlStatement)).getResultList();
	}

	private void isComment(String sqlFile) {

		if (isEmpty(sqlFile)) {
			throw new ContinueRuntimeException("because of empty line");
		}

		if (trim(sqlFile).startsWith("#")) {
			// comment
			throw new ContinueRuntimeException("because of comment line");
		}
	}

	private void systemOutWithStatement(List<BeanMap> resultList, String sqlstatement) {

		if (isEmpty(resultList)) {

			// 標準エラー出力
			System.err.println("select結果は0件です。" + "(SQL statement is" + trim(sqlstatement) + ")");

			throw new ContinueRuntimeException("resultList is empty");

		}

		System.out.println("select結果は" + resultList.size() + "件です。" + "(SQL statement is"
		        + trim(sqlstatement) + ")");

		systemOut(resultList);

	}

	private void systemOut(List<BeanMap> resultList) {
		for (BeanMap beanMap : resultList) {

			for (String key : beanMap.keySet()) {
				System.out.println("\t" + key + ": " + beanMap.get(key));
			}
		}
	}

	private void systemOutWithSQLFileSelectResult(List<BeanMap> resultList, String sqlFile) {

		if (isEmpty(resultList)) {

			// 標準エラー出力
			System.err.println("select結果は0件です。" + "(SQLFILE is " + trim(sqlFile) + ")");

			throw new ContinueRuntimeException("resultList is empty");

		}

		System.out.println("select結果は" + resultList.size() + "件です。" + "(SQLFILE is "
		        + trim(sqlFile) + ")");

		systemOut(resultList);

	}

	private String trim(String string) {

		if (string == null) {
			return null;
		}

		return string.trim();
	}

	private boolean isSelectSqlInside(String sqlFile) {
		return isSelectSql(sqlFile) && sqlFile.contains("_inside.sql");
	}

	private boolean isSelectSql(String sqlFile) {
		return sqlFile.startsWith("select") || sqlFile.startsWith("SELECT");
	}

	private boolean isSelectSqlNotInside(String sqlFile) {
		return isSelectSql(sqlFile) && !sqlFile.contains("_inside.sql");

	}

	private boolean isEmpty(List<BeanMap> resultList) {

		if (resultList == null) {
			return true;
		}

		return resultList.size() == 0;
	}

	boolean isIgnore(String sqlStatement) {
		return isEmpty(trim(sqlStatement)) || trim(sqlStatement).startsWith("--");
	}

	private boolean isEmpty(String sqlFile) {
		return StringUtils.isEmpty(trim(sqlFile));
	}

}
