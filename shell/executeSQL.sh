#! /bin/sh

CURR_DIR="`dirname $0`"
LIB_DIR=$CURR_DIR/sqltool.jar
CLASSPATH=$CURR_DIR:.:$LIB_DIR:$CLASSPATH:

java -cp $CLASSPATH -Djava.security.egd=file:///dev/urandom jp.co.so_net.tool.App
exit $?
